import 'phaser';

export default class {
  constructor(scene, cols, rows) {    
    var rotMap = new ROT.Map.Arena(cols, rows);    
    var mapData = [];    

    this.map = scene.make.tilemap({
      tileWidth: 24,
      tileHeight: 24,
      width: cols,
      height: rows
    });

    var tiles = this.map.addTilesetImage("world", null, 24, 24);    
    this.wallLayer = this.map.createBlankDynamicLayer("Ground", tiles); 
    this.wallLayer.setScale(2,2);

    var callback = function(x, y, value) {
      if (value) this.wallLayer.putTileAt(0, x, y);
    }.bind(this);
    rotMap.create(callback);
  
    this.wallLayer.setCollision(0);
  }
}