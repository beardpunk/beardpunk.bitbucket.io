import 'phaser';

export default class extends Phaser.Physics.Arcade.Sprite {

  constructor (scene, x, y, name, frame, velocity) {
    super(scene, x, y, name, frame);
    this.character = name;
    this.velocity = velocity;    
    scene.physics.world.enable(this);
    scene.add.existing(this);
    this.setCollideWorldBounds(true);

    this.generateAnim(scene, name, "left", "standing");
    this.generateAnim(scene, name, "right", "standing");
    this.generateAnim(scene, name, "up", "standing");
    this.generateAnim(scene, name, "down", "standing");

    this.generateAnim(scene, name, "left", "moving");
    this.generateAnim(scene, name, "right", "moving");
    this.generateAnim(scene, name, "up", "moving");
    this.generateAnim(scene, name, "down", "moving");

    this.generateAnim(scene, name, "left", "attacking");
    this.generateAnim(scene, name, "right", "attacking");
    this.generateAnim(scene, name, "up", "attacking");
    this.generateAnim(scene, name, "down", "attacking");

    this.direction = "right";
  }

  generateAnim(scene, character, direction, state) {
    var frameRates = {
      standing: 3,
      moving: 10,
      attacking: 12
    };

    var frameNames = scene.anims.generateFrameNames(character, {
      start: 1, end: 4,
      prefix: direction + "/" + state + "/", suffix: '.png'
    });

    scene.anims.create({ 
      key: character + "_" + direction + "_" + state,
      frames: frameNames, 
      frameRate: frameRates[state], 
      repeat: -1
    });
  }


  setDirection(x,y) {
    if (this.x < x - 10) {
      this.direction = "right";
      return;
    }
    if (this.x > x + 10) {
      this.direction = "left";
      return;
    }

    if (this.y < y) {
      this.direction = "down";
      return;
    }
    this.direction = "up";
  }

  attack(x, y) {    
    this.setVelocityX(0);
    this.setVelocityY(0);
    this.setDirection(x, y);  
    this.anims.play(this.character + "_" + this.direction + '_attacking', true);
  }  

  stand() {
    this.setVelocityX(0);
    this.setVelocityY(0);
    this.anims.play(this.character + "_" + this.direction + '_standing', true);
  }

  moveTo(x, y) {
    var scene = this.scene;


    // Calculate direction towards player
    var toPlayerX = x - this.x;
    var toPlayerY = y - this.y;

    // Normalize
    var toPlayerLength = Math.sqrt(toPlayerX * toPlayerX + toPlayerY * toPlayerY);
    toPlayerX = Math.round((toPlayerX / toPlayerLength) * 5);
    toPlayerY = Math.round((toPlayerY / toPlayerLength) * 5);
    
    

    for (var i = 0; i < this.scene.actors.length; i++) {
      var actor = this.scene.actors[i];
      if (actor == this) {        
        continue;
      }

      var target = new Phaser.Geom.Rectangle(this.getTopLeft().x + toPlayerX,
                                             this.getTopLeft().y + toPlayerY,
                                             this.width, this.height);

      if (Phaser.Geom.Rectangle.Overlaps(actor.getBounds(), target)) {              
        this.attack(actor.x, actor.y);
        return;        
      }
    }
    
    this.setDirection(x,y);
    this.anims.play(this.character + "_" + this.direction + '_moving', true);  
    scene.physics.moveTo(this, x, y, this.velocity, 0);        
  }
}