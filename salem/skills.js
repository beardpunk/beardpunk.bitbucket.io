
var skills = [
{
	title: "A Formal Education",
	unlock: ["Aristotelian Logic","Compacts &amp; Constitutions","Alchemy"]
}
,{
	title: "Acute Tree Harvesting",
	unlock: [""]
}
,{
	title: "Advanced Cooking",
	unlock: [""]
}
,{
	title: "Agriculture",
	unlock: ["Cabbage Growing","Cotton Planting","Maize Planting","Three-field System","Windmill Theory"]
}
,{
	title: "Alchemy",
	unlock: ["Pharmacology Formal","Steam Distillation"]
}
,{
	title: "Ambitious Excavation",
	unlock: [""]
}
,{
	title: "Aristotelian Logic",
	unlock: ["Theology","Pharmacology Formal","Lucky"]
}
,{
	title: "Arson",
	unlock: [""]
}
,{
	title: "Ascent to the Summit",
	unlock: [""]
}
,{
	title: "Backalley Pugilism",
	unlock: ["Fast Moves","Fencing","Clubbing","Polearms"]
}
,{
	title: "Baking",
	unlock: ["Tasty Pastries"]
}
,{
	title: "Bark Gathering",
	unlock: ["Flowers &amp; Berries","Nuts &amp; Seeds","Mushroom Hunting"]
}
,{
	title: "Beaver Skinning",
	unlock: [""]
}
,{
	title: "Big Game Hunting",
	unlock: ["Game Meats","Monster Hunting"]
}
,{
	title: "Blacksmithing",
	unlock: ["Cauldronmaking","Brazier Forging"]
}
,{
	title: "Botany",
	unlock: ["Forestry","Horticulture"]
}
,{
	title: "Brazier Forging",
	unlock: [""]
}
,{
	title: "Bug Hunting",
	unlock: ["Small Game Hunting"]
}
,{
	title: "Burial Rights",
	unlock: [""]
}
,{
	title: "Butcher's Thrift",
	unlock: ["Viscera &amp; Bits"]
}
,{
	title: "Butchery",
	unlock: ["Butcher's Thrift"]
}
,{
	title: "Cabbage Growing",
	unlock: ["Pumpkin Planting"]
}
,{
	title: "Cabinet Maker",
	unlock: [""]
}
,{
	title: "Carpentry",
	unlock: ["Pulleys &amp; Levers","Joinery &amp; Finish","Humble Abodes","Rustic Furniture","Simple Fences"]
}
,{
	title: "Cauldronmaking",
	unlock: [""]
}
,{
	title: "Cementation",
	unlock: [""]
}
,{
	title: "Ceramics &amp; Crucibles",
	unlock: ["Alchemy"]
}
,{
	title: "Childish Things",
	unlock: ["Survival Skills"]
}
,{
	title: "Clubbing",
	unlock: [""]
}
,{
	title: "Coaling",
	unlock: [""]
}
,{
	title: "Cobbling",
	unlock: ["Haberdashery"]
}
,{
	title: "Coffer Making",
	unlock: [""]
}
,{
	title: "Collector",
	unlock: ["Packrat"]
}
,{
	title: "Colonial Tradesmanship",
	unlock: ["Literacy","Settling","Pottery","Folk Medicine","Bug Hunting","Patchwork &amp; Rags","Essential Mineralogy","Whittling","Exploration","Simple Cooking","Coaling","Agriculture","Gardening"]
}
,{
	title: "Compacts &amp; Constitutions",
	unlock: ["Mercantilism"]
}
,{
	title: "Cotton Planting",
	unlock: ["Pumpkin Planting"]
}
,{
	title: "Elaborate Gemcutting",
	unlock: [""]
}
,{
	title: "Elementary Gemcutting",
	unlock: ["Tasteful Gemcutting"]
}
,{
	title: "Embroidery &amp; Silk",
	unlock: [""]
}
,{
	title: "Essential Mineralogy",
	unlock: ["Prospecting","Quarrying"]
}
,{
	title: "Exploration",
	unlock: ["Acute Tree Harvesting","Bark Gathering","Waterways"]
}
,{
	title: "Eye for an Eye",
	unlock: [""]
}
,{
	title: "Fast Moves",
	unlock: [""]
}
,{
	title: "Fencing",
	unlock: ["Firearms"]
}
,{
	title: "Field Dressing",
	unlock: ["Butchery"]
}
,{
	title: "Fine Leathercraft",
	unlock: [""]
}
,{
	title: "Firearms",
	unlock: [""]
}
,{
	title: "Fishing",
	unlock: ["Waterways"]
}
,{
	title: "Floriculture",
	unlock: ["Forestry"]
}
,{
	title: "Flowers &amp; Berries",
	unlock: ["Botany"]
}
,{
	title: "Folk Medicine",
	unlock: ["Potions &amp; Poultices","Rudimentary Triage "]
}
,{
	title: "Foraging",
	unlock: ["Indian Tracking"]
}
,{
	title: "Forestry",
	unlock: ["Shrub Orchards"]
}
,{
	title: "French Cuisine",
	unlock: ["Advanced Cooking"]
}
,{
	title: "Fried Foods",
	unlock: [""]
}
,{
	title: "Fungiculture",
	unlock: ["Horticulture"]
}
,{
	title: "Game Meats",
	unlock: ["Butcher's Thrift","Venison Cuisine"]
}
,{
	title: "Gardening",
	unlock: ["Floriculture","Vegetable Potting","Fungiculture"]
}
,{
	title: "Glass Blowing",
	unlock: [""]
}
,{
	title: "Granite Excavation",
	unlock: ["Ambitious Excavation"]
}
,{
	title: "Grave Robbing",
	unlock: [""]
}
,{
	title: "Green Thumb",
	unlock: [""]
}
,{
	title: "Haberdashery",
	unlock: [""]
}
,{
	title: "Handheld Explosives",
	unlock: [""]
}
,{
	title: "Hideworking",
	unlock: [" Beaver Skinning","Tanning"]
}
,{
	title: "Hiking",
	unlock: ["Hill Climbing","Swimming","Labouring"]
}
,{
	title: "Hill Climbing",
	unlock: ["Mountaineering"]
}
,{
	title: "Hoarder",
	unlock: [""]
}
,{
	title: "Horticulture",
	unlock: ["Shrub Orchards"]
}
,{
	title: "Humble Abodes",
	unlock: ["Mercantilism"]
}
,{
	title: "Indian Tracking",
	unlock: [""]
}
,{
	title: "Intermediate Cooking",
	unlock: ["French Cuisine","Venison Cuisine","Baking","Smoked Meats"]
}
,{
	title: "Iron Amalgamation",
	unlock: [""]
}
,{
	title: "Joinery &amp; Finish",
	unlock: ["Coffer Making","Sophisticated Furniture"]
}
,{
	title: "Kiln Construction",
	unlock: [""]
}
,{
	title: "Labouring",
	unlock: [""]
}
,{
	title: "Lace &amp; Fancywork",
	unlock: ["Embroidery &amp; Silk","Pockets"]
}
,{
	title: "Larceny",
	unlock: ["Grave Robbing","Waste"]
}
,{
	title: "Literacy",
	unlock: ["A Formal Education"]
}
,{
	title: "Locksmithing",
	unlock: ["Mechanics"]
}
,{
	title: "Lore of The Lumberwoods",
	unlock: ["Hiking","Locksmithing","Foraging"]
}
,{
	title: "Lucky",
	unlock: [""]
}
,{
	title: "Maize Planting",
	unlock: [""]
}
,{
	title: "Masonry",
	unlock: ["Humble Abodes","Mortar Masonry","Windmill Theory"]
}
,{
	title: "Mechanics",
	unlock: ["Pulleys &amp; Levers"]
}
,{
	title: "Mercantilism",
	unlock: [""]
}
,{
	title: "Metallurgy",
	unlock: ["Mechanics","Metalsmithing"]
}
,{
	title: "Metalsmithing",
	unlock: ["Weapon Forging","Cementation"]
}
,{
	title: "Mineral Sifting",
	unlock: [""]
}
,{
	title: "Mining",
	unlock: [""]
}
,{
	title: "Monster Hunting",
	unlock: ["Viscera &amp; Bits"]
}
,{
	title: "Mortar Masonry",
	unlock: [""]
}
,{
	title: "Mountaineering",
	unlock: ["Ascent to the Summit"]
}
,{
	title: "Mushroom Hunting",
	unlock: ["Slug Hunting"]
}
,{
	title: "Nuts &amp; Seeds",
	unlock: ["Botany"]
}
,{
	title: "Packrat",
	unlock: ["Hoarder"]
}
,{
	title: "Patchwork &amp; Rags",
	unlock: ["Sewing","Weaving","Cobbling"]
}
,{
	title: "Pharmacology Formal",
	unlock: [""]
}
,{
	title: "Plantation Management",
	unlock: [""]
}
,{
	title: "Pockets",
	unlock: [""]
}
,{
	title: "Polearms",
	unlock: [""]
}
,{
	title: "Potato Growing",
	unlock: [""]
}
,{
	title: "Potions &amp; Poultices",
	unlock: ["Pharmacology Formal"]
}
,{
	title: "Pottery",
	unlock: ["Kiln Construction","Masonry","Cauldronmaking"]
}
,{
	title: "Prospecting",
	unlock: ["Mining"]
}
,{
	title: "Pulleys &amp; Levers",
	unlock: ["Windmill Theory"]
}
,{
	title: "Pumpkin Planting",
	unlock: [""]
}
,{
	title: "Quarrying",
	unlock: ["Granite Excavation"]
}
,{
	title: "Revenge",
	unlock: ["Eye for an Eye"]
}
,{
	title: "Rudimentary Triage",
	unlock: [""]
}
,{
	title: "Rustic Furniture",
	unlock: ["Sophisticated Furniture"]
}
,{
	title: "Sanctioned Mortification",
	unlock: [""]
}
,{
	title: "Seafood Chef",
	unlock: [""]
}
,{
	title: "Seamanship",
	unlock: [""]
}
,{
	title: "Self-Defense",
	unlock: ["Backalley Pugilism","Big Game Hunting","The Rights of Englishmen"]
}
,{
	title: "Settling",
	unlock: ["Locksmithing","Simple Fences","Compacts &amp; Constitutions","Humble Abodes","Seamanship","Pulleys &amp; Levers"]
}
,{
	title: "Sewing",
	unlock: ["Lace &amp; Fancywork"]
}
,{
	title: "Shrub Orchards",
	unlock: [""]
}
,{
	title: "Simple Cooking",
	unlock: ["Intermediate Cooking","Fried Foods","Seafood Chef"]
}
,{
	title: "Simple Fences",
	unlock: ["Mortar Masonry"]
}
,{
	title: "Slug Hunting",
	unlock: ["Turkey Farming"]
}
,{
	title: "Small Game Hunting",
	unlock: ["Field Dressing","Hideworking","Big Game Hunting","Turkey Farming"]
}
,{
	title: "Smoked Meats",
	unlock: ["Advanced Cooking"]
}
,{
	title: "Sophisticated Furniture",
	unlock: ["Cabinet Maker"]
}
,{
	title: "Steam Distillation",
	unlock: [""]
}
,{
	title: "Survival Skills",
	unlock: ["Collector","Colonial Tradesmanship","Fishing","Lore of The Lumberwoods","Self-Defense"]
}
,{
	title: "Swimming",
	unlock: ["Waterways","Labouring"]
}
,{
	title: "Sympathetic Magic",
	unlock: [""]
}
,{
	title: "Tanning",
	unlock: ["Fine Leathercraft"]
}
,{
	title: "Tasteful Gemcutting",
	unlock: ["Elaborate Gemcutting"]
}
,{
	title: "Tasty Pastries",
	unlock: ["Advanced Cooking"]
}
,{
	title: "The Rights of Englishmen",
	unlock: ["Brazier Forging","Compacts &amp; Constitutions","Firearms","Revenge","Sanctioned Mortification","The Story of Cain &amp; Abel","Trespassing"]
}
,{
	title: "The Story of Cain &amp; Abel",
	unlock: [""]
}
,{
	title: "Theology",
	unlock: [""]
}
,{
	title: "Three-field System",
	unlock: [""]
}
,{
	title: "Torches",
	unlock: [""]
}
,{
	title: "Trespassing",
	unlock: ["Larceny"]
}
,{
	title: "Turkey Farming",
	unlock: [""]
}
,{
	title: "Vegetable Potting",
	unlock: ["Shrub Orchards"]
}
,{
	title: "Venison Cuisine",
	unlock: ["Advanced Cooking","Smoked Meats"]
}
,{
	title: "Viscera &amp; Bits",
	unlock: [""]
}
,{
	title: "Waste",
	unlock: ["Arson"]
}
,{
	title: "Waterways",
	unlock: ["Seamanship"]
}
,{
	title: "Weapon Forging",
	unlock: [""]
}
,{
	title: "Weaving",
	unlock: [""]
}
,{
	title: "Whittling",
	unlock: ["Carpentry","Locksmithing"]
}
,{
	title: "Windmill Theory",
	unlock: [""]
}
];

/*
var skills = [
	{title: "A",
	 unlock: ["B", "C", "D"]
	},
	{title: "B",
 	 unlock: ["E", "F"]
	},
	{title: "C",
 	 unlock: [""]
	},
	{title: "D",
 	 unlock: [""]
	},
	{title: "E",
 	 unlock: [""]
	},
	{title: "F",
 	 unlock: [""]
	},
	{title: "G",
 	 unlock: ["F"]
	}
];
*/