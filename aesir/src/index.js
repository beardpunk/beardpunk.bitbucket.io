import 'phaser';
import Creature from './Creature';
import Map from './Map';

var config = {
    type: Phaser.AUTO,
    width: 800,
    height: 600,
    physics: {
        default: 'arcade',
        arcade: {            
            debug: false
        }
    },
    scene: {
        preload: preload,
        create: create,
        update: update
    }
};

var game = new Phaser.Game(config);

var player, enemy, cursors, wasd, map, camera, shift;

function preload () {
    this.load.multiatlas('aesir', 'assets/aesir.json', 'assets');    
    this.load.multiatlas('goblin', 'assets/goblin.json', 'assets');    
    this.load.image("world", 'assets/world.png');
}

function create () {
    this.actors = [];
    map = new Map(this, 20, 20);
    player = new Creature(this, 100, 100, 'aesir', 'right/standing/1.png', 200);    
    enemy = new Creature(this, 400, 400, 'goblin', 'right/standing/1.png', 100);        

    this.actors.push(player);
    this.actors.push(enemy);

    camera = this.cameras.main;
    camera.startFollow(player);    
    this.physics.world.setBounds(0, 0, map.map.widthInPixels * 2, map.map.heightInPixels * 2);
    camera.setBounds(-10, -10, map.map.widthInPixels * 2 + 10, map.map.heightInPixels * 2 + 10);

    this.physics.add.collider(player, map.wallLayer);
    this.physics.add.collider(enemy, map.wallLayer);
    
    cursors = this.input.keyboard.createCursorKeys();
    wasd = {
        up: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W),
        down: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.S),
        left: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A),
        right: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D)
    };

    shift = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SHIFT)

    game.input.mouse.capture = true;
}

function update () {    

    enemy.moveTo(player.x, player.y);

    if (this.input.activePointer.isDown) {
        var x = this.input.x + this.cameras.main.scrollX, 
            y = this.input.y + this.cameras.main.scrollY
        if (shift.isDown) {
            player.attack(x, y);
            return;
        }
        player.moveTo(x, y, null, 750);
        return;
    }
    
    player.stand();
}